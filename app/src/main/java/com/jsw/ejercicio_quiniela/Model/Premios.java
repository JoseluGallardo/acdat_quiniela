package com.jsw.ejercicio_quiniela.Model;

/**
 * Created by joselu on 13/01/17.
 */

public class Premios {
    private String apuestaGanadora;
    private String ganado;
    private String premio;

    public Premios() {
    }

    public String getApuestaGanadora() {
        return apuestaGanadora;
    }

    public void setApuestaGanadora(String apuestaGanadora) {
        this.apuestaGanadora = apuestaGanadora;
    }

    public String getGanado() {
        return ganado;
    }

    public void setGanado(String ganado) {
        this.ganado = ganado;
    }

    public String getPremio() {
        return premio;
    }

    public void setPremio(String premio) {
        this.premio = premio;
    }
}
