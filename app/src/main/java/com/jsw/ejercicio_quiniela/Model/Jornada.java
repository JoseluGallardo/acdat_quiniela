package com.jsw.ejercicio_quiniela.Model;

import java.util.ArrayList;

/**
 * Created by joselu on 13/01/17.
 */

public class Jornada {
    String nJornada;
    String recaudacion;

    String quince;
    String catorce;
    String trece;
    String doce;
    String once;
    String diez;

    String resultado;

    //Un premio equivale a una apuesta y a la cantidad que ha acertado con respecto al resultado
    ArrayList<Premios> listaPremios;

    public Jornada() {
        listaPremios = new ArrayList<>();
    }

    public Jornada(String nJornada, String recaudacion, String quince, String catorce, String trece, String doce, String once, String diez, String resultado, ArrayList<Premios> listaApuestas) {
        this.nJornada = nJornada;
        this.recaudacion = recaudacion;
        this.quince = quince;
        this.catorce = catorce;
        this.trece = trece;
        this.doce = doce;
        this.once = once;
        this.diez = diez;
        this.resultado = resultado;
        this.listaPremios = listaApuestas;
    }

    public String getnJornada() {
        return nJornada;
    }

    public void setnJornada(String nJornada) {
        this.nJornada = nJornada;
    }

    public String getRecaudacion() {
        return recaudacion;
    }

    public void setRecaudacion(String recaudacion) {
        this.recaudacion = recaudacion;
    }

    public String getQuince() {
        return quince;
    }

    public void setQuince(String quince) {
        this.quince = quince;
    }

    public String getCatorce() {
        return catorce;
    }

    public void setCatorce(String catorce) {
        this.catorce = catorce;
    }

    public String getTrece() {
        return trece;
    }

    public void setTrece(String trece) {
        this.trece = trece;
    }

    public String getDoce() {
        return doce;
    }

    public void setDoce(String doce) {
        this.doce = doce;
    }

    public String getOnce() {
        return once;
    }

    public void setOnce(String once) {
        this.once = once;
    }

    public String getDiez() {
        return diez;
    }

    public void setDiez(String diez) {
        this.diez = diez;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public ArrayList<Premios> getListaPremios() {
        return listaPremios;
    }

    public void setListaApuestas(ArrayList<Premios> listaApuestas) {
        this.listaPremios = listaApuestas;
    }
}
