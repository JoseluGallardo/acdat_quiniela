package com.jsw.ejercicio_quiniela;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.jsw.ejercicio_quiniela.Model.Jornada;
import com.jsw.ejercicio_quiniela.Utils.IO;
import com.jsw.ejercicio_quiniela.Utils.Logica;
import com.jsw.ejercicio_quiniela.Utils.RestClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    static String[] mListApuestas;
    @BindView(R.id.layout)
    LinearLayout mLayout;
    @BindView(R.id.rg_radios)
    RadioGroup mRadios;
    @BindView(R.id.til_apuestas)
    TextInputLayout mApuestas;
    @BindView(R.id.til_resultados)
    TextInputLayout mResultados;
    @BindView(R.id.til_premios)
    TextInputLayout mPremios;
    @BindView(R.id.btn_calcular)
    Button mCalcular;
    ProgressDialog pg;
    boolean esXML = true;
    boolean apuestasOK = false;
    ArrayList<Jornada> mListJornadas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (calcularURLS()) {
                    apuestasOK = false;
                    descargarFicheros();
                } else
                    snackbar("Formato de URL incorrecto");
            }
        });
    }

    private boolean calcularURLS() {
        boolean res = false;
        String apuestas = mApuestas.getEditText().getText().toString();
        String resultados = mResultados.getEditText().getText().toString();
        String premios = mPremios.getEditText().getText().toString();
        String extension = "";

        if (mRadios.getCheckedRadioButtonId() == R.id.rb_xml) {
            extension = ".xml";
            esXML = true;
        } else {
            extension = "json";
            esXML = false;
        }

        try {

            if (apuestas.substring(apuestas.length() - 4, apuestas.length()).equals(".txt"))
                if (premios.substring(premios.length() - 4, premios.length()).equals(extension))
                    if (resultados.substring(resultados.length() - 4, resultados.length()).equals(extension))
                        res = true;
        } catch (Exception ex) {
            //
        }
        return res;
    }

    private void descargarFicheros() {

        pg = new ProgressDialog(this);
        pg.setCancelable(false);
        pg.setTitle("Descargando");
        pg.setMessage("Descargando archivos necesarios");
        pg.show();

        RestClient.get(mApuestas.getEditText().getText().toString(), new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                pg.dismiss();
                snackbar(throwable.getLocalizedMessage());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                pg.dismiss();
                apuestasOK = true;
                mListApuestas = Logica.parsearApuestas(responseBody);
            }
        });


        RestClient.get(mResultados.getEditText().getText().toString(), new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                pg.dismiss();
                snackbar(throwable.getLocalizedMessage());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                pg.dismiss();
                if (apuestasOK) {
                    try {
                        if (esXML)
                            mListJornadas = Logica.parsearXML(responseBody);
                        else
                            mListJornadas = Logica.parsearJSON(responseBody);

                        Logica.compruebaPremios(mListJornadas, mListApuestas);

                        guardarPremios();

                    } catch (Exception ex) {
                        snackbar(ex.getLocalizedMessage());
                    }
                } else {
                    snackbar("Not found");
                }
            }
        });


    }

    private void guardarPremios() {
        String res;
        try {
            if (esXML)
                res = IO.escribirXml(mListJornadas, mPremios.getEditText().getText().toString());
            else
                res = IO.escribirJson(mListJornadas, mPremios.getEditText().getText().toString());

            snackbar(res);

            subirResultados();

        } catch (Exception ex) {
            snackbar(ex.getMessage());
        }
    }

    private void snackbar(String text) {
        Snackbar.make(mLayout, text, Snackbar.LENGTH_SHORT).show();
    }

    private void subirResultados() {

        File file;
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Subiendo al servidor...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        RequestParams params = new RequestParams();
        try {
            file = new File(Environment.getExternalStorageDirectory(), mPremios.getEditText().getText().toString());

            if (!file.exists())
                file.createNewFile();

            params.put("premios", file);
            RestClient.post("http://alumno.club/superior/joseluis/upload.php", params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    snackbar(throwable.getLocalizedMessage());
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    progressDialog.dismiss();
                    snackbar("Fichero guardado y subido correctamente.");
                }
            });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            snackbar(e.getLocalizedMessage());
            progressDialog.dismiss();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
