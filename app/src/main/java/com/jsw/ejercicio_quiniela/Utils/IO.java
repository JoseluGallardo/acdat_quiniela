package com.jsw.ejercicio_quiniela.Utils;

import android.os.Environment;
import android.util.Xml;

import com.jsw.ejercicio_quiniela.Model.Jornada;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Created by joselu on 14/01/17.
 */

public class IO {

    public static String escribirXml(ArrayList<Jornada> listJornadas, String salida) throws IOException {

        String res;
        File file = new File(Environment.getExternalStorageDirectory(), salida);
        if (!file.exists())
            file.createNewFile();
        FileOutputStream fos = new FileOutputStream(file);
        XmlSerializer serializer = Xml.newSerializer();

        try {
            serializer.setOutput(fos, "UTF-8");
            serializer.startDocument(null, true);
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            String jornada;
            String premio;

            serializer.startTag(null, "Jornadas");
            for (int i = 0; i < listJornadas.size(); i++) {

                for (int j = 0; j < listJornadas.get(i).getListaPremios().size(); j++) {

                    serializer.startTag(null, "Jornada");
                    serializer.attribute(null, "numero", String.valueOf(i + 1));

                    serializer.startTag(null, "apuesta");
                    serializer.text(listJornadas.get(i).getListaPremios().get(j).getApuestaGanadora());
                    serializer.endTag(null, "apuesta");

                    serializer.startTag(null, "premio");
                    serializer.text(listJornadas.get(i).getListaPremios().get(j).getGanado());
                    serializer.endTag(null, "premio");

                    serializer.startTag(null, "ganado");
                    serializer.text(listJornadas.get(i).getListaPremios().get(j).getPremio());
                    serializer.endTag(null, "ganado");

                    serializer.endTag(null, "Jornada");
                }
            }

            serializer.endTag(null, "Jornadas");
            res = "Guardado con exito en: " + salida;
        } catch (Exception ex) {
            res = "Error al guardar los ficheros...";
        } finally {
            if (serializer != null)
                serializer.flush();
            if (fos != null)
                fos.close();

        }

        return res;
    }


    public static String escribirJson(ArrayList<Jornada> listaDeJornadas, String salida) throws JSONException {
        JSONObject raiz = new JSONObject();
        JSONArray quiniela = new JSONArray();
        String res;

        for (int i = 0; i < listaDeJornadas.size(); i++) {
            for (int j = 0; j < listaDeJornadas.get(i).getListaPremios().size(); j++) {
                JSONObject jornada = new JSONObject();
                jornada.put("jornada", listaDeJornadas.get(i).getnJornada());
                jornada.put("apuesta", listaDeJornadas.get(i).getListaPremios().get(j).getApuestaGanadora());
                jornada.put("premio", listaDeJornadas.get(i).getListaPremios().get(j).getGanado());
                jornada.put("ganado", listaDeJornadas.get(i).getListaPremios().get(j).getPremio());
                quiniela.put(jornada);
            }
            ;
        }
        raiz.put("Quiniela", quiniela);


        try {
            File file = new File(Environment.getExternalStorageDirectory(), salida);
            if (!file.exists())
                file.createNewFile();

            FileOutputStream fos = new FileOutputStream(file);
            OutputStreamWriter ous = new OutputStreamWriter(fos);

            ous.write(raiz.toString());
            ous.flush();
            fos.close();
            ous.close();
            res = "Guardado con exito en: " + salida;
        } catch (Exception ex) {
            res = "Archivo JSON no guardado...";
        }

        return res;
    }
}
