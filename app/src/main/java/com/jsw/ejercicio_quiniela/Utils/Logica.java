package com.jsw.ejercicio_quiniela.Utils;

import android.util.Xml;

import com.jsw.ejercicio_quiniela.Model.Jornada;
import com.jsw.ejercicio_quiniela.Model.Premios;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by joselu on 13/01/17.
 */

public class Logica {


    public static String[] parsearApuestas(String response) {
        return response.split("\n");
    }

    public static ArrayList<Jornada> parsearXML(String response) throws XmlPullParserException, IOException {
        ArrayList<Jornada> result = new ArrayList<>();

        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new StringReader(response));
        int eventType = xpp.getEventType();

        Jornada jornada = null;

        while ((eventType = xpp.next()) != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (xpp.getName().equals("quiniela")) {
                        jornada = new Jornada();
                        jornada.setResultado("");

                        String recaudacion = xpp.getAttributeValue(2);

                        if (recaudacion.substring(recaudacion.length() - 4, recaudacion.length()).equals("0000")) {
                            return result;
                        }

                        jornada.setnJornada(xpp.getAttributeValue(0));
                        jornada.setRecaudacion(xpp.getAttributeValue(2));
                        jornada.setQuince(xpp.getAttributeValue(3));
                        jornada.setCatorce(xpp.getAttributeValue(4));
                        jornada.setTrece(xpp.getAttributeValue(5));
                        jornada.setDoce(xpp.getAttributeValue(6));
                        jornada.setOnce(xpp.getAttributeValue(7));
                        jornada.setDiez(xpp.getAttributeValue(8));
                    }
                    if (xpp.getName().equals("partit")) {
                        if (jornada != null) {
                            jornada.setResultado(jornada.getResultado() + xpp.getAttributeValue("", "sig"));
                        }
                    }
                    break;
                case XmlPullParser.TEXT:
                    break;
                case XmlPullParser.END_TAG:
                    if (xpp.getName().equals("quiniela"))
                        if (jornada != null)
                            result.add(jornada);
                    break;
            }
        }

        return result;
    }

    public static ArrayList<Jornada> parsearJSON(String response) throws JSONException {
        ArrayList<Jornada> result = new ArrayList<>();

        Jornada jornada;

        JSONObject JSON = new JSONObject(response);
        JSONObject quinielista = JSON.getJSONObject("quinielista");
        JSONArray quiniela = quinielista.getJSONArray("quiniela");

        for (int i = 0; i < quiniela.length(); i++) {
            jornada = new Jornada();
            jornada.setResultado("");
            JSONObject jrnd = quiniela.getJSONObject(i);

            String recaudacion = jrnd.getString("-recaudacion");
            if (recaudacion.substring(recaudacion.length() - 4, recaudacion.length()).equals("0000"))
                break;

            jornada.setnJornada(jrnd.getString("-jornada"));
            jornada.setRecaudacion(recaudacion);
            jornada.setQuince(jrnd.getString("-el15"));
            jornada.setCatorce(jrnd.getString("-el14"));
            jornada.setTrece(jrnd.getString("-el13"));
            jornada.setDoce(jrnd.getString("-el12"));
            jornada.setOnce(jrnd.getString("-el11"));
            jornada.setDiez(jrnd.getString("-el10"));

            JSONArray partido = jrnd.getJSONArray("partit");

            for (int j = 0; j < 15; j++) {
                JSONObject prtt = partido.getJSONObject(j);
                String hola = prtt.getString("-sig");
                //String holo = jornada.getRe
                jornada.setResultado(jornada.getResultado() + prtt.getString("-sig"));
            }

            result.add(jornada);
        }


        return result;
    }

    public static void compruebaPremios(ArrayList<Jornada> listJornadas, String[] listApuestas) {
        for (int i = 0; i < listJornadas.size(); i++) {

            for (String apuesta : listApuestas) {
                int coincidencias = 0;

                String uno = listJornadas.get(i).getResultado();

                for (int k = 0; k < 14; k++) {
                    if (uno.charAt(k) == apuesta.charAt(k)) {
                        coincidencias++;
                    }
                }

                if (coincidencias == 14) {
                    String final_uno = String.valueOf(uno.charAt(14)) + String.valueOf(uno.charAt(15));
                    String final_dos = String.valueOf(apuesta.charAt(14)) + String.valueOf(apuesta.charAt(15));

                    if (final_uno.contains(final_dos)) {
                        coincidencias++;
                    }
                }

                if (coincidencias >= 10) {
                    Premios prm = new Premios();
                    prm.setApuestaGanadora(apuesta);
                    prm.setGanado(String.valueOf(coincidencias));
                    listJornadas.get(i).getListaPremios().add(prm);

                    switch (coincidencias) {
                        case 10:
                            String holo = listJornadas.get(i).getDiez();
                            prm.setPremio(listJornadas.get(i).getDiez());
                            break;
                        case 11:
                            prm.setPremio(listJornadas.get(i).getOnce());
                            break;
                        case 12:
                            prm.setPremio(listJornadas.get(i).getDoce());
                            break;
                        case 13:
                            prm.setPremio(listJornadas.get(i).getTrece());
                            break;
                        case 14:
                            prm.setPremio(listJornadas.get(i).getCatorce());
                            break;
                        case 15:
                            prm.setPremio(listJornadas.get(i).getQuince());
                            break;
                    }
                }
            }
        }
    }
}
