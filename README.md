# Ejercicio Quiniela #

La pantalla principal necesita el enlace al fichero de apuestas en formato .txt, y el fichero de resultados en formato .xml o .json.

![Screenshot_1484483292.png](https://bitbucket.org/repo/Kjq7GM/images/2929189229-Screenshot_1484483292.png)



Mientras la descarga es realizada avisamos al usuario:

![Screenshot_1484483382.png](https://bitbucket.org/repo/Kjq7GM/images/2624400542-Screenshot_1484483382.png)




Cuando la descarga y la comprobación es correcta, se descarga en local y se sube al server:

![Screenshot_1484483398.png](https://bitbucket.org/repo/Kjq7GM/images/759261397-Screenshot_1484483398.png)


Fichero Local:

![Screenshot_1484483429.png](https://bitbucket.org/repo/Kjq7GM/images/274370887-Screenshot_1484483429.png)



Fichero en la web:

![Captura de pantalla de 2017-01-15 13-43-07.png](https://bitbucket.org/repo/Kjq7GM/images/2166495500-Captura%20de%20pantalla%20de%202017-01-15%2013-43-07.png)